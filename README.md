# A bunch of quickly-thrown-together specs

In the unlikely event you stumble on this little corner of the gits...

Hi! I've uploaded this bunch of pre-written docker-compose configurations, with what I *hope* is relatively obvious ways to configure them, in the faint hope that it can be helpful to someone.


## Why?

Because in some cases it took me A Bit™ to find and/or pull the right containers and have the right mix of software and configurations.


## All right, what's here then?

### [Matrix/Riot](matrix)

Imagine Slack, but opensauced and federated/decentralized. A *true* heir to the IRC crown, just with a truckload of extra steps.

### [Plex/Jellyfin/Kodi](media)

Truthfully I only use Plex right now, but I did haphazardly throw together kodi and Jellyfin configurations in the event Plex support becomes a bunch of toilets.

### [Portainer](portainer)

A REALLY neat way to manage your Docker stuffs without much effort

### [QBittorrentVPN](qbittorrentvpn)

👀

### [Jitsi](jitsi)

Because Google management is becoming money-driven instead of tech-driven, let's vote with our money (or lack thereof) and use tech that does exactly what they do, but without them. BLEAAH.

### [Mastodon](mastodon)

Imagine Twitter. Now change the interface. And make it a web standard. Boom, you have Mastodon, one of the main [ActivityPub](https://www.w3.org/TR/activitypub/) implementations.

The link goes to my "personal" fork, which is actually pretty literally just me forcing a fetch of the hub docker images instead of a build.
