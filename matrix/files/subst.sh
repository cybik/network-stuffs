#! /bin/sh

# Clear out
rm -rf /etc/nginx/conf.d/*.conf

export VARS='$MAIN_TLD:$RIOT_SUB_TLD:$MATRIX_SUB_TLD:$MAIN_RIOT_PORT:$MAIN_EXPOSED_PORT:$MAIN_IDENTITY_PORT:$MAIN_NERDS_ARE_12_PORT:$MAIN_FEDERATED_EXTERNAL_PORT:$MAIN_FEDERATED_INTERNAL_PORT'

# Subst-gen
for FILE in $(find /etc/nginx/conf.d.templates -name *.conf); do
	echo "Processing $FILE"
	envsubst "$VARS" < $FILE > /etc/nginx/conf.d/$(basename $FILE)
done

# (Re?)Prepare well-known
rm -rf /srv/well-known
mkdir -p /srv/well-known/.well-known/matrix
for FILE in $(ls /srv/well-known.templates); do
	echo "Processing $FILE"
	envsubst "$VARS" < /srv/well-known.templates/$FILE > /srv/well-known/.well-known/matrix/$FILE
done

# Start
echo "Starting nginx"
nginx -g 'daemon off;'
